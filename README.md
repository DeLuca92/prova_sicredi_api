## Prova Sicredi API

Este projeto foi desenvolvido em Java TestNG e RestAssured, tendo com objetivo realizar o teste da API solicitada conforme os seguintes parâmetros:

 - Consulta de um CEP válido ( Retorna  CEP, logradouro, complemento, bairro, localidade, uf e ibge.)
 - Consulta de um CEP inexistente ( Retorna um atributo erro)
 - Consulta de um CEP inválido ( Retorna um atributo erro)
 - Consulta do retorno da API (Extra) ( Retorna um array com CEP, logradouro, complemento, bairro, localidade, uf e ibge )

# Pré-requisitos

Este projeto conta com a versão JavaSE 1.8 e TestNG.

Caso você não possua o TestNG instalado no Eclipse, basta adicioná-lo por meio do botão "Help" presente no menu superior da IDE, e, em seguida, clique em "Install new software". Procure pelo plugin, selecione-o e clique em "Finish".

# Execução

Abra o projeto dentro do Eclipse por meio de File > Import > Existing Maven Project. Em seguida, selecione o diretório do projeto e clique em "Finish".
Uma vez com o projeto aberto dentro do Eclipse, clique com o botão direito do mouse em cima do arquivo "testeNG.xml" e selecione "Run As" > "TestNG Suite".