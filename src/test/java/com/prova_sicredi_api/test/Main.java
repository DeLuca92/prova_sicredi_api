package com.prova_sicredi_api.test;


import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;
import static org.hamcrest.Matchers.equalTo;


import org.hamcrest.Matchers;
import org.testng.annotations.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class Main {


	@Test
	public void verificaCepValido()
	{
	
		given().
		when().
			get( "https://viacep.com.br/ws/91060900/json/").
		then()
			.assertThat().body("cep", Matchers.equalTo("91060-900"))
			.assertThat().body("logradouro", Matchers.equalTo("Avenida Assis Brasil 3940"))
			.assertThat().body("complemento", Matchers.equalTo(""))
			.assertThat().body("bairro", Matchers.equalTo("S�o Sebasti�o"))
			.assertThat().body("localidade", Matchers.equalTo("Porto Alegre"))
			.assertThat().body("uf", Matchers.equalTo("RS"))
			.assertThat().body("ibge", Matchers.equalTo("4314902"));
	}
    
	
	@Test
	public void verificadaCepInexistente()
	{
		
		given().
		when().
			get( "https://viacep.com.br/ws/91060909/json/").
		then().body("erro", equalTo(true)).statusCode(200);
		
	}
	
	@Test
	public void verificadaCepInvalido()
	{
	
		given().
		when().
			get( "https://viacep.com.br/ws/d/json/").
		then().statusCode(400);
		
	}
    
	@Test
	public void DesafioExtra()
	{
		JsonArray jsonArray = new JsonArray();
		//Get the response as jsonArray.
		jsonArray = given().baseUri("https://viacep.com.br/ws/RS/Gravatai/Barroso/json/")
		.get().as(JsonArray.class);
		
				
		
			JsonObject jsonObject = jsonArray.get(0).getAsJsonObject();
			assertEquals(jsonObject.get("cep").getAsString(), "94085-170");
			assertEquals(jsonObject.get("logradouro").getAsString(), "Rua Ari Barroso");
			assertEquals(jsonObject.get("complemento").getAsString(), "");
			assertEquals(jsonObject.get("bairro").getAsString(), "Morada do Vale I");
			assertEquals(jsonObject.get("localidade").getAsString(), "Gravata�");
			assertEquals(jsonObject.get("uf").getAsString(), "RS");
			assertEquals(jsonObject.get("unidade").getAsString(), "");
			assertEquals(jsonObject.get("ibge").getAsString(), "4309209");
			assertEquals(jsonObject.get("gia").getAsString(), "");
		
			
	
			JsonObject jsonObject2 = jsonArray.get(1).getAsJsonObject();
			assertEquals(jsonObject2.get("cep").getAsString(), "94175-000");
			assertEquals(jsonObject2.get("logradouro").getAsString(), "Rua Almirante Barroso");
			assertEquals(jsonObject2.get("complemento").getAsString(), "");
			assertEquals(jsonObject2.get("bairro").getAsString(), "Recanto Corcunda");
			assertEquals(jsonObject2.get("localidade").getAsString(), "Gravata�");
			assertEquals(jsonObject2.get("uf").getAsString(), "RS");
			assertEquals(jsonObject2.get("unidade").getAsString(), "");
			assertEquals(jsonObject2.get("ibge").getAsString(), "4309209");
			assertEquals(jsonObject2.get("gia").getAsString(), "");
		
	
	
		
		
		
		
	}
	
}

    